const express = require('express');
const morgan = require('morgan');
const path = require('path');
const app = express();

const products = [
    {
        id: 1,
        name: 'Laptop'
    },
    {
        id: 2,
        name: 'Johnny'
    }
];

app.get('/products', (req,res) => {
    res.json(products);
})

app.set('port', process.env.PORT || 3000)

app.use(express.static(path.join(__dirname, 'public')));

app.listen(app.get('port'), () => {
    console.log(`Server en el puerto ${app.get('port')}`);
});