/**$(function(){
    $('#getProducts').on('click',function (){
        $.ajax({
            url: '/products',
            success: function(products){
                let tbody = $('tbody');
                tbody.html('');
                products.forEach(product => {
                    tbody.append(`
                        <tr>
                            <td class="id">${product.id}</td>
                            <td>
                                <input type="text" class="name" value="${product.name}"/>
                            </td>
                            <td>
                                <button class="update-button">Editar</button>
                                <button class="delete-button">Eliminar</button>
                            </td>
                        </tr>
                    `)
                })
            }
        })
    })
}) 
*/

$(document).ready(function (){
        $.ajax({
            url: 'https://api.openbrewerydb.org/breweries',
            success: function(breweries){
                let tbody = $('tbody');
                tbody.html('');
                for (let i = 0; i < 10; i++) {
                    tbody.append(`
                        <tr>
                            <td class="id">${breweries[i].id}</td>
                            <td>${breweries[i].name}</td>
                            <td><button type="button" class="btn btn-primary""
                            id="details-button">Detalles</button></td>
                        </tr>   
                `)
            }
        }
    });
});
    